# Simple app with login with checkbox

This simulates a login page with a checkbox that needs to be checked in order to login.

![Alt text](image.png)

The ***DAST_BROWSER_PATH_TO_LOGIN_FORM*** variable can be used. It represents a comma-separated list of selectors representing elements to click on prior to entering the DAST_USERNAME and DAST_PASSWORD into the login form. 

Authentication is simulated using javascript (in /src/login.html), and authentication will only be successfull if the username and password combination is correct, and if the "I agree to the term" checkbox is checked. 

If ***DAST_BROWSER_PATH_TO_LOGIN_FORM*** is not set, the DAST job will fail. 
